package no.uib.smo015.info381.foodItem;

import java.util.Map;

/**
 * Created by Sindre on 27.03.2017.
 */
public class FoodItem {
    private String id, name, category;
    private Map<String, Attribute> attributes;

    public FoodItem(String id, String name, String category, Map<String, Attribute> attributes) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.attributes = attributes;
    }

    //TODO Note: Ser ut som den faktisk skriver ut \n istedet for newlines som vi faktisk ønsket
    public String toString() {
        String info = this.name + " with id: " + this.id + " is a member of " + this.category  + " and has attributes: \n";
        for(Attribute attr : attributes.values()) {
            info += attr.getName() + " = " + attr.getValue() + attr.getUnit() + "\n";
        }
        return info;
    }

    public Attribute getAttribute(String key) {
        return this.attributes.get(key);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Map<String, Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Attribute> attributes) {
        this.attributes = attributes;
    }
}
