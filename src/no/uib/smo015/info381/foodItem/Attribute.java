package no.uib.smo015.info381.foodItem;

/**
 * Created by Sindre on 27.03.2017.
 */
public class Attribute {
    private String name, unit, value;

    public Attribute(String name, String unit, String value) {
        this.name = name;
        this.unit = unit;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
