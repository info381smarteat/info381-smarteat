package no.uib.smo015.info381.foodItem;

/**
 * Created by Sindre on 27.04.2017.
 */
public enum MealType {
    BREAKFAST, LUNCH, DINNER, SUPPER, SNACK;
}
