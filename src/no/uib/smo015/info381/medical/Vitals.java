package no.uib.smo015.info381.medical;

/**
 * Created by Sindre on 04.04.2017.
 */
public class Vitals {
    // bloodSugarLevel = millimol pr. liter blod (forkortet mmol/L)
    private double bloodSugarLevel, hasCarbAmount, hasFatAmount, hasProteinAmount, hasKiloCalorieAmount, hasKilojouleAmount;

    public Vitals(double bloodSugarLevel, double hasCarbAmount, double hasFatAmount, double hasProteinAmount, double hasKiloCalorieAmount, double hasKilojouleAmount) {
        this.bloodSugarLevel = bloodSugarLevel;
        this.hasCarbAmount = hasCarbAmount;
        this.hasFatAmount = hasFatAmount;
        this.hasProteinAmount = hasProteinAmount;
        this.hasKiloCalorieAmount = hasKiloCalorieAmount;
        this.hasKilojouleAmount = hasKilojouleAmount;
    }

    public double getBloodSugarLevel() {
        return bloodSugarLevel;
    }

    public void setBloodSugarLevel(double bloodSugarLevel) {
        this.bloodSugarLevel = bloodSugarLevel;
    }

    public double getHasCarbAmount() {
        return hasCarbAmount;
    }

    public void setHasCarbAmount(double hasCarbAmount) {
        this.hasCarbAmount = hasCarbAmount;
    }

    public double getHasFatAmount() {
        return hasFatAmount;
    }

    public void setHasFatAmount(double hasFatAmount) {
        this.hasFatAmount = hasFatAmount;
    }

    public double getHasProteinAmount() {
        return hasProteinAmount;
    }

    public void setHasProteinAmount(double hasProteinAmount) {
        this.hasProteinAmount = hasProteinAmount;
    }

    public double getHasKiloCalorieAmount() {
        return hasKiloCalorieAmount;
    }

    public void setHasKiloCalorieAmount(double hasKiloCalorieAmount) {
        this.hasKiloCalorieAmount = hasKiloCalorieAmount;
    }

    public double getHasKilojouleAmount() {
        return hasKilojouleAmount;
    }

    public void setHasKilojouleAmount(double hasKilojouleAmount) {
        this.hasKilojouleAmount = hasKilojouleAmount;
    }
}
