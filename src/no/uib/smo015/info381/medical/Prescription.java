package no.uib.smo015.info381.medical;

import org.apache.jena.rdf.model.Resource;

public class Prescription extends Vitals{

	private Resource prescribedTo; // This field is not being used at the moment - Can possibly lead to fatal errors such as nullPointerException
	private String ID;
	
	public Prescription(String id, double bloodSugarLevel, double hasCarbAmount, double hasFatAmount, double hasProteinAmount,
			double hasKiloCalorieAmount, double hasKilojuleAmount, Resource prescribedTo) {
		super(bloodSugarLevel, hasCarbAmount, hasFatAmount, hasProteinAmount, hasKiloCalorieAmount, hasKilojuleAmount);
		this.ID = id;
		this.prescribedTo = prescribedTo;
	}

	public Prescription(String id, double bloodSugarLevel, double hasCarbAmount, double hasFatAmount, double hasProteinAmount,
						double hasKiloCalorieAmount, double hasKilojuleAmount) {
		super(bloodSugarLevel, hasCarbAmount, hasFatAmount, hasProteinAmount, hasKiloCalorieAmount, hasKilojuleAmount);
		this.ID = id;
	}


	
	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}


	/**
	 * @return the prescribedTo
	 */
	public Resource getPrescribedTo() {
		return prescribedTo;
	}

	/**
	 * @param prescribedTo the prescribedTo to set
	 */
	public void setPrescribedTo(Resource prescribedTo) {
		this.prescribedTo = prescribedTo;
	}


	public void setID(String ID) {
		this.ID = ID;
	}

	//TODO MISSING PRESCRIBED TO
	@Override
	public String toString(){
		return getID() + " is a prescription, with max blood suger level: " + getBloodSugarLevel() + ". With max carbs: " + getHasCarbAmount() +
				", with max fat: " + getHasFatAmount() + ", with max protein: " + getHasProteinAmount() + ". Max KCAL: " + getHasKiloCalorieAmount() +
				", MAX kJ:" + getHasKiloCalorieAmount();

	}
	
	
}
