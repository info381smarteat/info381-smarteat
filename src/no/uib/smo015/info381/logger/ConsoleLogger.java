package no.uib.smo015.info381.logger;

/**
 * Created by Sindre on 28.03.2017.
 */
public class ConsoleLogger implements Logger {

    @Override
    public void log(String text) {
        System.out.println(text);
    }
}
