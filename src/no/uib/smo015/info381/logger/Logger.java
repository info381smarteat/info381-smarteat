package no.uib.smo015.info381.logger;

/**
 * Created by Sindre on 28.03.2017.
 */
public interface Logger {
    public void log(String text);
}
