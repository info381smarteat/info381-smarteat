package no.uib.smo015.info381.parser;

import no.uib.smo015.info381.foodItem.Attribute;
import no.uib.smo015.info381.foodItem.FoodItem;
import no.uib.smo015.info381.logger.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sindre on 27.03.2017.
 */
public class Parser {
    private Logger logger;
    private List<FoodItem> items;

    public Parser(Logger logger) {
        this.logger = logger;
        this.items = new ArrayList();
    }

    public List<FoodItem> parseFile(String filename) {
        logger.log("Trying to parse " + filename);
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

            // Two first lines are not interesting, just read them and carry on
            br.readLine();
            br.readLine();

            logger.log("\nReading headers...");
            String[] headers = br.readLine().split("\t");


            logger.log("Headers read...");
            logger.log("\nReading units...");
            String[] units = br.readLine().split("\t");
            logger.log("Units read...");
            String line = "";
            while((line = br.readLine()) != null) {

                String[] columns = line.split("\t");
                if(columns.length <= 3) {
                    continue;
                }
                Map<String, Attribute> attr = generateAttributes(headers, units, columns);
                items.add(new FoodItem(columns[0].toLowerCase(), columns[2], this.getCategory(Integer.parseInt(columns[1])), attr));

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.log("\nParsed " + items.size() + " matvarer\n");
        return items;
    }

    private String getCategory(int id) {
        switch (id) {
            case 1:
                return "Dairy products";
            case 2:
                return "Dairy products";
            case 3:
                return "Egg products";
            case 4:
                return "Meat";
            case 5:
                return "Diverse meals";
            case 6:
                return "Fish and seafood";
            case 7:
                return "Wheat and bread";
            case 8:
                return "Fruit and vegetables";
            case 9:
                return "Sugar and sweet products";
            case 10:
                return "Diverse products";
            case 11:
                return "Drinks";
            case 12:
                return "Alcohol";
            case 13:
                return "Desserts";
            case 14:
                return "Snacks";
            case 15:
                return "Vegetarian";
            case 16:
                return "Ingredients";
            case 17:
                return "Food for babies";
            default:
                return "No category present";
        }
//        String[] idParts = id.split("\\.");
//        if(idParts.length == 0) {
//            return "Diverse";
//        }
//        if(idParts[0].equalsIgnoreCase("01")) {
//            return "Dairy products";
//        } else if(idParts[0].equalsIgnoreCase("02")) {
//            return "Egg products";
//        } else if(idParts[0].equalsIgnoreCase("03")) {
//            return "Meat";
//        } else if(idParts[0].equalsIgnoreCase("04")) {
//            return "Fish and seafood";
//        } else if(idParts[0].equalsIgnoreCase("05")) {
//            return "Wheat and bread";
//        } else if(idParts[0].equalsIgnoreCase("06")) {
//            return "Fruit and vegetables";
//        } else if(idParts[0].equalsIgnoreCase("07")) {
//            return "Diverse products";
//        } else if(idParts[0].equalsIgnoreCase("08")) { // Majones og dressing
//            return "Diverse products";
//        } else if(idParts[0].equalsIgnoreCase("09")) {
//            return "Drinks";
//        } else if(idParts[0].equalsIgnoreCase("10")) {
//            return "Diverse meals";
//        } else if(idParts[0].equalsIgnoreCase("11")) {
//            return "Food for babies";
//        } else {
//            return "Unknown category";
//        }
    }

    private Map<String, Attribute> generateAttributes(String[] headers, String[] units, String[] columns) {

        Map<String, Attribute> attributes = new HashMap<>();
        for(int i = 0; i < columns.length; i++) {
            Attribute attr = new Attribute(headers[i], units[i], columns[i]);
            attributes.put(headers[i], attr);
        }
        return attributes;
    }
}
