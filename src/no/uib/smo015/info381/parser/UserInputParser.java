package no.uib.smo015.info381.parser;


import no.uib.smo015.info381.medical.Prescription;
import no.uib.smo015.info381.ontology.PrescriptionOntology;
import no.uib.smo015.info381.user.User;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by krist on 24.04.2017.
 */
public class UserInputParser {

    private final PrescriptionOntology prescriptionOntology;
    List<User> users;
    List<Prescription> prescriptions;

    public UserInputParser(PrescriptionOntology prescriptionOntology) {
        users = new ArrayList<>();
        prescriptions = new ArrayList<>();
        this.prescriptionOntology = prescriptionOntology;
    }

    /**
     * Loops through the excel document
     * @param fileName
     * @throws IOException
     */
    public Map<String, List> parseTheExcelDocument(String fileName) throws IOException {
        POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(new File(fileName)));
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row;
        HSSFCell cell;

        int rows;
        rows = sheet.getPhysicalNumberOfRows();

        int cols = 0;
        int tmp;


        for (int i = 0; i < 10 || i < rows; i++) {
            row = sheet.getRow(i);
            if (row != null) {
                tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                if (tmp > cols) cols = tmp;
            }
        }


        for (int r = 0; r < rows; r++) {
            row = sheet.getRow(r);
            if (row != null) {
                for (int c = 2; c < cols; c++) {
                    cell = row.getCell((short) c);

                    //Only get data from cells that contain something
                    if (cell != null && !cell.toString().isEmpty()) {
                        if(cell.getRowIndex() == 13 && cell.toString().equalsIgnoreCase("END")){

                            Map<String, List> usersAndPrescriptions = new HashMap<>();
                            usersAndPrescriptions.put("users", users);
                            usersAndPrescriptions.put("prescriptions", prescriptions);
                            return usersAndPrescriptions;

                        } else {
                            handleData(cell);
                        }
                    }

                }
            }
        }
        return null;
    }

    /**
     * Row 0, 8 and 15 is empty, column 1 and 2 contains data we don't want to parse
     *
     * @param cell
     */
    public void handleData(HSSFCell cell) {
        int row = cell.getRowIndex();
        int col = cell.getColumnIndex() - 2; //-2 because the two first columns in the document are not data, col will match index in list of users/prescriptions
        String cellTxt = cell.toString();
        //System.out.println("R:" + row + " C:" + col + " | " + cell.toString());
        User user = new User();

        //TODO Prescription ID? Er det brukerens id? Eller hva er det?
        //TODO Hvis ID'en skal settes i excel må vi legge til en ekstra rad i dokumentet og en ekstra switch, men det er EAZYPEAZY

        Prescription prescription = null;
        if(!users.isEmpty() && row != 1) {
            user = users.get(col);
            prescription = new Prescription("pres_" + user.getSsn(), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        } //If not first entry for user, get from list
        if(!prescriptions.isEmpty() && row != 8){ prescription = prescriptions.get(col); } //If not first entry for prescription, get from list

            switch (row) {
                case 1: //Creates new user and sets name
                    user.setName(cellTxt);
                    users.add(user);
                    break;
                case 2: //Sets SSN on that user
                    user.setSsn(cellTxt.substring(0, 6));
                    break;
                case 3: //Sets gender for the user
                    user.setGenderNoEnum(cellTxt);
                    break;
                case 4: //Adds medical conditions to user
                    List<String> conditions = new ArrayList<>();
                    String[] conditionsSplit = cell.toString().split(",");
                    for(String condition : conditionsSplit){
                        conditions.add(condition);
                    }
                    user.setMedicalConditions(conditions);
                    break;
                case 5: //Sets the weight of the user
                    user.setWeight(Double.valueOf(cellTxt));
                    break;
                case 6: //Sets the age
                    Double age = Double.valueOf(cellTxt);
                    user.setAge(age.intValue());
                    break;

                //---------------- USER ABOVE, PRESCRIPTION BELOW --------------------

                case 8: //Sets the bloodsugar level
                    //TODO Kunne ha satt prescribedTo, hvis prescribedTo hadde tatt en user og ikke resource
                    //TODO Hadde da blitt: prescription.setPrescribedTo(user);
                    prescription.setBloodSugarLevel(Double.valueOf(cell.toString()));
                    //prescription.setPrescribedTo(user); //SEE TODOS
                    prescriptions.add(prescription);
                    break;
                case 9: //Sets carb amount for the prescription
                    prescription.setHasCarbAmount(Double.valueOf(cellTxt));
                    break;
                case 10: //Sets fat amount
                    prescription.setHasFatAmount(Double.valueOf(cellTxt));
                    break;
                case 11: //Sets protein amount
                    prescription.setHasProteinAmount(Double.valueOf(cellTxt));
                    break;
                case 12: //Sets kcal amount
                    prescription.setHasKiloCalorieAmount(Double.valueOf(cellTxt));
                    break;
                case 13: //sets kJ amount
                    prescription.setHasKilojouleAmount(Double.valueOf(cellTxt));
                    break;
                default: break;
            }
        }

    /**
     * Prints out the data we have parse on users and prescriptions
     */
    public void printData() {
        System.out.println("\n -------------------------------------------------------- PARSED FROM EXCEL DOCUMENT ------------------------------------------------------");
        for (User user : users) {
            System.out.println(user.toString());
        }
        System.out.println("");
        for (Prescription pres : prescriptions) {
            System.out.println(pres.toString());
        }
    }
}

