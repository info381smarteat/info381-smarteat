package no.uib.smo015.info381.query;

import no.uib.smo015.info381.foodItem.FoodItem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Sindre on 28.03.2017.
 */
public class QueryModule {
    private List<FoodItem> items;

    public QueryModule(List<FoodItem> items) {
        this.items = items;
    }

    public QueryModule() {
        this.items = new ArrayList<>();
    }

    public List<FoodItem> getItems() {
        return items;
    }

    public void setItems(List<FoodItem> items) {
        this.items = items;
    }

    public Stream<FoodItem> filterCategory(String category) {
        return this.items.stream().filter(food -> food.getCategory().equalsIgnoreCase(category));
    }
}
