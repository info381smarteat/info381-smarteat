package no.uib.smo015.info381.main;

import no.uib.smo015.info381.logger.ConsoleLogger;
import no.uib.smo015.info381.parser.Parser;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;

/**
 * Created by Sindre on 27.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        new Creator(ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM), new Parser(new ConsoleLogger())).go();
    }


}
