package no.uib.smo015.info381.main;

import no.uib.smo015.info381.foodConsumption.DailyFoodConsumption;
import no.uib.smo015.info381.foodConsumption.Meal;
import no.uib.smo015.info381.foodItem.FoodItem;
import no.uib.smo015.info381.foodItem.MealType;
import no.uib.smo015.info381.medical.Prescription;
import no.uib.smo015.info381.ontology.FoodOntology;
import no.uib.smo015.info381.ontology.MealOntology;
import no.uib.smo015.info381.ontology.PrescriptionOntology;
import no.uib.smo015.info381.ontology.UserOntology;
import no.uib.smo015.info381.parser.Parser;
import no.uib.smo015.info381.parser.UserInputParser;
import no.uib.smo015.info381.query.QueryModule;
import no.uib.smo015.info381.user.User;
import no.uib.smo015.info381.user.UserFactory;
import no.uib.smo015.info381.user.UserVitalGenerator;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class binds everything together, with one simple go-method for executing all the code
 * Created by Sindre on 19.04.2017.
 */
public class Creator {

    private OntModel model;
    private Parser parser;
    private List<FoodItem> eatenFoods;
    private DailyFoodConsumption dfc;
    private List<FoodItem> mettesEatenFood;
    private DailyFoodConsumption mettesDfc;
    private List<User> users;
    private UserFactory userFactory;
    private List<FoodItem> items;
    private QueryModule queryModule;
//    private static final String FILE_NAME = "Matvaretabellen for parsing - Ark 1.tsv";
    private static final String FILE_NAME = "Matvaretabellen 2016 (Excel) (1) - Matvaretabell for parsing.tsv";

    public Creator(OntModel model, Parser parser) {
        this.model = model;
        this.parser = parser;
        this.queryModule = new QueryModule();
    }

    public void go() {

        List<String> blacklist = new ArrayList<>();
        blacklist.add("Food for babies");
        items = parser.parseFile(Creator.FILE_NAME).stream().filter(item -> !blacklist.contains(item.getCategory())).collect(Collectors.toList());;

        Map<String, String> namespaces = new HashMap<>();
        namespaces.put("smarteat", "http://smarteat.com/ontology/");


        UserInputParser userInputParser = new UserInputParser(new PrescriptionOntology(this.model));
        Map<String, List> usersAndPrescriptions = null;
        try {
            usersAndPrescriptions = userInputParser.parseTheExcelDocument("DataInputUsersPrescriptionsExcel.xls");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // TODO Randomize the choosing of foods

        FoodOntology foodOntology = new FoodOntology(model);
        UserOntology userOntology = new UserOntology(model);
        PrescriptionOntology prescriptionOntology = new PrescriptionOntology(model);
        MealOntology mealOntology = new MealOntology(model);

        loadExternalOntology("smarteat_rdfs.owl");
        foodOntology.setNameSpaces(namespaces);

        items.forEach(item -> foodOntology.setPropertyValues(item));

        List<User> listOfUsers = usersAndPrescriptions.get("users");
        List<Prescription> listOfPrescriptions = usersAndPrescriptions.get("prescriptions");

        for(int i = 0; i < listOfPrescriptions.size(); i++) {
            User user = listOfUsers.get(i);
            Prescription pres = listOfPrescriptions.get(i);

            user.setUserVitals(UserVitalGenerator.createVitals());
            user.getUserVitals().setBloodSugarLevel(pres.getBloodSugarLevel());
            user.setDailyFoodConsumption(createDailyFoodConsumption());

            Resource userRes = userOntology.createUser(user);
            foodOntology.updateMealsForUser(userRes, user);
            prescriptionOntology.createPrescription(pres, user);

        }


//        foodOntology.printModel();

        foodOntology.saveModel("data.ttl", "TURTLE");
    }



    private DailyFoodConsumption createDailyFoodConsumption() {
        Date today = new Date();
        this.queryModule.setItems(items);
        List<FoodItem> dishes = this.queryModule.filterCategory("Diverse retter, produkter og ingredienser").collect(Collectors.toList());

        List<Meal> meals = dishes.stream().map(dish -> new Meal(dish.getName(), dishes.subList(0, 6), new Date(), dish.getName(), MealType.DINNER)).collect(Collectors.toList());

        return new DailyFoodConsumption(today, meals);
    }



    private void loadExternalOntology(String filename) {
        try {
            FileInputStream inputStream = new FileInputStream(filename);
            this.model.read(inputStream, "smarteat");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
