package no.uib.smo015.info381.foodConsumption;

import no.uib.smo015.info381.foodItem.FoodItem;
import no.uib.smo015.info381.foodItem.MealType;

import java.util.Date;
import java.util.List;

/**
 * Created by Sindre on 27.04.2017.
 */
public class Meal {
    private String name;
    private List<FoodItem> foodItems;
    private Date date;
    private String id;
    private Enum<MealType> typeOfMeal;

    public Meal(String name, List<FoodItem> foodItems, Date date, String id, MealType typeOfMeal) {
        this.name = name;
        this.foodItems = foodItems;
        this.date = date;
        this.id = id;
        this.typeOfMeal = typeOfMeal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(List<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Enum<MealType> getTypeOfMeal() {
        return typeOfMeal;
    }

    public void setTypeOfMeal(Enum<MealType> typeOfMeal) {
        this.typeOfMeal = typeOfMeal;
    }
}
