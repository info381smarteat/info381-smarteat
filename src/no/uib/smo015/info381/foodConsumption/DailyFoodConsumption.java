package no.uib.smo015.info381.foodConsumption;

import no.uib.smo015.info381.foodItem.FoodItem;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Sindre on 04.04.2017.
 */
public class DailyFoodConsumption {
	private Date date;
	private double hasWaterAmount, hasTotalCalories, hasTotalCarbs, hasTotalProteins, hasTotalFat;
	private List<Meal> listOfMeals;

	public DailyFoodConsumption(Date date,  List<Meal> listOfMeals) {
		this.date = date;
		this.listOfMeals = listOfMeals;
		updateFields();
	}

	public void addFoodToDailyList(Meal meal){
		listOfMeals.add(meal);

		updateFields();
	}

	private void updateFields(){
		this.hasTotalCalories = 0;
		this.hasTotalCarbs = 0;
		this.hasTotalProteins  = 0;
		this.hasTotalFat = 0;
		this.hasWaterAmount = 0;
		
		for(Meal meal : listOfMeals){
			for(FoodItem food : meal.getFoodItems()) {
				Set<String> foodSet = food.getAttributes().keySet();

				for(String attri : foodSet){

					double parsedValue = 0;

					try{
						parsedValue = Double.parseDouble(food.getAttribute(attri).getValue());
					}catch(NumberFormatException e){
						parsedValue = 0;
					}

					updateField(attri, parsedValue);

				}
			}
		}
	}

	//TODO denne metoden er litt statisk, men er litt usikker p� hvordan vi ellers kan gj�re det
	public void updateField(String fieldId, double value){
		if(fieldId.equalsIgnoreCase("Fett")){
			this.hasTotalFat += value;
		}
		else if(fieldId.equalsIgnoreCase("Kilojoule")){
			this.hasTotalCalories += value;
		}
		else if(fieldId.equalsIgnoreCase("Karbohydrater")){
			this.hasTotalCarbs += value;
		}
		else if(fieldId.equalsIgnoreCase("Proteiner")){
			this.hasTotalProteins += value;
		}
		else if(fieldId.equalsIgnoreCase("Vann")){
			this.hasWaterAmount += value;
		}
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getHasWaterAmount() {
		return hasWaterAmount;
	}

	public double getHasTotalCalories() {
		return hasTotalCalories;
	}

	public double getHasTotalCarbs() {
		return hasTotalCarbs;
	}


	public double getHasTotalProteins() {
		return hasTotalProteins;
	}

	public double getHasTotalFat() {
		return hasTotalFat;
	}

	public List<Meal> getListOfMeals() {
		return listOfMeals;
	}
}
