package no.uib.smo015.info381.ontology;

import org.apache.jena.ontology.OntModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * Created by Sindre on 18.04.2017.
 */
public class Ontology {
    public final static String BASE_URL = "http://smarteat.com/ontology/";
    private OntModel model;

    public Ontology(OntModel model) {
        this.model = model;
    }

    public void printModel() {
        this.model.write(System.out, "TURTLE");
    }

    public void saveModel(String filename, String format) {
        try {
            this.model.write(new FileOutputStream(new File(filename)), format);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setNameSpaces(Map<String, String> ns) {
        this.model.setNsPrefixes(ns);
    }

    public OntModel getModel() {
        return model;
    }

    public void setModel(OntModel model) {
        this.model = model;
    }
}

