package no.uib.smo015.info381.ontology;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;

import no.uib.smo015.info381.medical.Prescription;
import no.uib.smo015.info381.user.User;

public class PrescriptionOntology extends Ontology{
	
	private OntClass prescribedDietClass;
	
	
	public PrescriptionOntology(OntModel model) {
		super(model);
		prescribedDietClass = super.getModel().createClass(BASE_URL + "prescribedDiet");
	}
	
	public void createPrescription(Prescription prescription, User user){
		Resource presRes = this.prescribedDietClass.createIndividual(BASE_URL + prescription.getID());
		setProperties(presRes, prescription);
		
		presRes.addProperty(super.getModel().createProperty(BASE_URL + "prescribedTo"),
				super.getModel().getResource(BASE_URL + user.getSsn()));
	}
		
	public void setProperties(Resource res, Prescription pres){
		
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxBloodSugarLevel"), pres.getBloodSugarLevel());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxCarbAmount"), pres.getHasCarbAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxFatAmount"), pres.getHasFatAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxProteinAmount"), pres.getHasProteinAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxKiloCalorieAmount"), pres.getHasKiloCalorieAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "maxKilojouleAmount"), pres.getHasKilojouleAmount());
		
	}
	
}
