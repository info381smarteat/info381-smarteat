package no.uib.smo015.info381.ontology;

import no.uib.smo015.info381.user.User;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDFS;


/**
 * Created by krist on 18.04.2017.
 */
public class UserOntology extends Ontology {
    private OntClass userClass;
    private Property unitProp;
    private Property weightProp;
    private Property ageProp;

    public UserOntology(OntModel model) {
        super(model);
        this.userClass = super.getModel().createClass(BASE_URL + "User");

    }

    public Resource createUser(User user){
        Resource userRes = this.userClass.createIndividual(BASE_URL + user.getSsn());
        setProperties(userRes, user);
        return userRes;
    }

    public void setProperties(Resource res, User user){
        res.addLiteral(super.getModel().createProperty(BASE_URL + "Weight"), user.getWeight());

        res.addLiteral(RDFS.label, user.getName());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "ssn"), user.getSsn());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "Age"), user.getAge());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "Gender"), user.getGender().toString());


        // TODO Koble high blood glucose correction factor and target blood glucose to the user through the excel spreadsheet and add to the ontology

        //vitals - values that the user has
        res.addLiteral(super.getModel().createProperty(BASE_URL + "BloodSugarLevel"), user.getUserVitals().getBloodSugarLevel());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "CarbAmount"), user.getUserVitals().getHasCarbAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "FatAmount"), user.getUserVitals().getHasFatAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "ProteinAmount"), user.getUserVitals().getHasProteinAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "KiloCalorieAmount"), user.getUserVitals().getHasKiloCalorieAmount());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "KilojouleAmount"), user.getUserVitals().getHasKilojouleAmount());

        
        //consumption (per meal. )
        res.addLiteral(super.getModel().createProperty(BASE_URL + "Date"), user.getDailyFoodConsumption().getDate().toString());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "HasTotalCalories"), user.getDailyFoodConsumption().getHasTotalCalories());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "HasTotalCarbs"), user.getDailyFoodConsumption().getHasTotalCarbs());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "HasTotalFat"), user.getDailyFoodConsumption().getHasTotalFat());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "HasTotalProteins"), user.getDailyFoodConsumption().getHasTotalProteins());
        res.addLiteral(super.getModel().createProperty(BASE_URL + "HasWaterAmount"), user.getDailyFoodConsumption().getHasWaterAmount());

    }

}
