package no.uib.smo015.info381.ontology;

import no.uib.smo015.info381.foodItem.Attribute;
import no.uib.smo015.info381.foodItem.FoodItem;
import no.uib.smo015.info381.user.User;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

/**
 * Created by Sindre on 17.04.2017.
 */
public class FoodOntology extends Ontology {
    private OntClass foodItemClass, nutritionalContentClass, mealClass;
    private Property unitProp;

    public FoodOntology(OntModel model) {
        super(model);
        createClasses();
    }

    private void createClasses() {
        this.foodItemClass = super.getModel().createClass(BASE_URL + "foodItem");
        this.mealClass = super.getModel().createClass(BASE_URL + "mealItem");
        this.nutritionalContentClass = super.getModel().createClass(BASE_URL + "buildingBlock");
    }

    /*
        This method creates all the individual properties for a given foodItem
        It also creates an individual from the foodItem-class and populates the values for a given foodItem with
        the values from Matvaretabellen.
        @param FoodItem foodItem the foodItem you want to create an instance of
    */
    public void setPropertyValues(FoodItem foodItem) {
        //TODO refactor
        Resource edibleResource = this.foodItemClass.createIndividual(BASE_URL + foodItem.getAttribute("MatvareID").getValue());

        // for each key in the attributes
        foodItem.getAttributes().keySet().forEach(key -> {
            Attribute attr = foodItem.getAttributes().get(key);
            String cleanedName = cleanName(attr.getName());
            if (!attr.getUnit().equalsIgnoreCase("")) { // Some attributes such as Matvare (the name of the foodItem) has no unit, so there is no need to create that triple

                Property property = super.getModel().createProperty(BASE_URL + cleanedName);

                unitProp = super.getModel().createProperty(BASE_URL + "unit");
                property.addLiteral(unitProp, attr.getUnit());
                property.addProperty(RDF.type, super.getModel().getResource(nutritionalContentClass.getURI()));

                Object edibleResourceLiteral = createLiteralValue(attr);
                edibleResource.addLiteral(property, edibleResourceLiteral);
                edibleResource.addLiteral(RDFS.label, foodItem.getName());
                edibleResource.addLiteral(super.getModel().createProperty(BASE_URL + "id"), foodItem.getId());
                edibleResource.addProperty(super.getModel().createProperty(BASE_URL + "category"), foodItem.getCategory());
            }

        });
    }


    public Object createLiteralValue(Attribute attr) {
        String value = replaceCommaWithPeriod(attr.getValue());

        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return value;
        }

    }

    private String replaceCommaWithPeriod(String doubleAsString) {
        return doubleAsString.replace(",", ".");
    }


    // Method for replacing different characters with underline. This to ensure compatibility with food items with weird names and Jena
    private String cleanName(String name) {
        return name.replaceAll("[ +:,\\-\\(\\)]", "_");
    }


    private String formatResourceValues(FoodItem foodItem, String key) {
        Attribute attr = foodItem.getAttribute(key);
        // TODO Sjekk for null-value
        return attr.getValue() + attr.getUnit();
    }

    public void updateMealsForUser(Resource userRes, User user) {
        user.getDailyFoodConsumption().getListOfMeals().forEach(item -> {
            Resource mealRes = this.mealClass.createIndividual(BASE_URL + item.getTypeOfMeal().toString() + "_" + user.getSsn());

            mealRes.addLiteral(RDFS.label, item.getName());
            mealRes.addLiteral(super.getModel().createProperty(BASE_URL + "date"), item.getDate().toString());
            Property prop = super.getModel().createProperty(BASE_URL + "mealContains");
            item.getFoodItems().forEach(foodItem -> {
                mealRes.addProperty(prop, super.getModel().getResource(BASE_URL + foodItem.getId()));
            });
            // TODO Legg til ressurser for meals - dato, navn, osv...
            userRes.addProperty(super.getModel().createProperty(BASE_URL + "hasEaten"), mealRes);
        });
    }
}
