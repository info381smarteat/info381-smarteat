package no.uib.smo015.info381.user;

/**
 * Created by Sindre on 18.04.2017.
 */
public enum Genders {
    MALE, FEMALE, SHEMALE, TRANS, FLUID, ATTACKHELICOPTER, ANDROID, OTHER;
}
