package no.uib.smo015.info381.user;

import no.uib.smo015.info381.foodConsumption.DailyFoodConsumption;
import no.uib.smo015.info381.medical.Prescription;
import no.uib.smo015.info381.utils.Utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.apache.jena.atlas.lib.RandomLib.random;

/**
 * Created by Sindre on 21.04.2017.
 */
public class UserFactory {
    private UserVitalGenerator userVitalGenerator;

    public UserFactory(UserVitalGenerator userVitalGenerator) {
        this.userVitalGenerator = userVitalGenerator;
    }

    public User createUser(String name, Genders gender, DailyFoodConsumption dfc) {
        String ssn = createSsn();
        return new User(name, ssn, gender, createMedicalConditions(), Utils.randomDouble(80, 120), Utils.randomInt(12,16), userVitalGenerator.createVitals(), dfc, createPrescriptions(ssn));
    }

    private String createSsn() {
        return new BigInteger(130, random).toString(32);
    }

    private List<String> createMedicalConditions() {
        List<String> conditions = new ArrayList<>();
        conditions.add("Diabetes");
        return conditions;
    }

    private List<Prescription> createPrescriptions(String id) {
        List<Prescription> prescriptions = new ArrayList<>();
        Prescription pres = new Prescription("pres_" + id, Utils.randomInt(100,120), Utils.randomDouble(2300, 2500), Utils.randomDouble(159.7, 190.6), Utils.randomDouble(30, 40), Utils.randomDouble(45, 50), Utils.randomDouble(600, 650));
        prescriptions.add(pres);
        return prescriptions;
    }

}
