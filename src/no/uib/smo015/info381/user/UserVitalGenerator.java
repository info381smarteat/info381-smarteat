package no.uib.smo015.info381.user;

import no.uib.smo015.info381.medical.Vitals;
import no.uib.smo015.info381.utils.Utils;

/**
 * Created by Sindre on 21.04.2017.
 */
public class UserVitalGenerator {

    public static Vitals createVitals() {
        return new Vitals(Utils.randomDouble(100, 110), Utils.randomDouble(50, 100), Utils.randomDouble(500, 1000), Utils.randomDouble(250, 500), Utils.randomDouble(500, 1500), Utils.randomDouble(700, 900));
    }
}
