package no.uib.smo015.info381.user;

import no.uib.smo015.info381.foodConsumption.DailyFoodConsumption;
import no.uib.smo015.info381.medical.Prescription;
import no.uib.smo015.info381.medical.Vitals;

import java.util.List;

/**
 * Created by Sindre on 04.04.2017.
 */
public class User {
    private String name;
    private String ssn;
    private Enum<Genders> gender;
    private List<String> medicalConditions;
    private double weight;
    private int age;
    private Vitals userVitals;
    private DailyFoodConsumption dailyFoodConsumption;
    private List<Prescription> prescriptions;


    public User(){

    }

    public User(String name, String ssn, Genders gender, List<String> medicalConditions, double weight, int age,
                Vitals userVitals, DailyFoodConsumption dailyFoodConsumption, List<Prescription> prescriptions){
        this.name = name;
        this.ssn = ssn;
        this.gender = gender;
        this.medicalConditions = medicalConditions;
        this.weight = weight;
        this.age = age;
        this.userVitals = userVitals;
        this.dailyFoodConsumption = dailyFoodConsumption;
        this.prescriptions = prescriptions;
    }

    public Vitals getUserVitals() {
        return userVitals;
    }

    public void setUserVitals(Vitals userVitals) {
        this.userVitals = userVitals;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMedicalConditions() {
        return medicalConditions;
    }

    public void setMedicalConditions(List<String> medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Vitals getMedicalPrescription() {
        return userVitals;
    }

    public void setMedicalPrescription(Vitals userVitals) {
        this.userVitals = userVitals;
    }

    public DailyFoodConsumption getDailyFoodConsumption() {
        return dailyFoodConsumption;
    }

    public void setDailyFoodConsumption(DailyFoodConsumption dailyFoodConsumption) {
        this.dailyFoodConsumption = dailyFoodConsumption;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Enum<Genders> getGender() {
        return gender;
    }

    public void setGender(Enum<Genders> gender) {
        this.gender = gender;
    }

    public void setGenderNoEnum(String sex){
        if(sex.equalsIgnoreCase("M") || sex.equalsIgnoreCase("MALE")){
            setGender(Genders.MALE);
        } else if(sex.equalsIgnoreCase("F") || sex.equalsIgnoreCase("FEMALE")){
            setGender(Genders.FEMALE);
        }
        else{
            setGender(Genders.OTHER);
        }
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    //TODO Mangler prescriptions and such
    @Override
    public String toString(){
        return getName() + " is a " + getGender() + " with SSN: " + getSsn() + ", at age " + getAge() +
                ", and weighs in at " + getWeight() + " kgs. " + getName() + " has these medical conditions: " + getMedicalConditions();
    }
}


