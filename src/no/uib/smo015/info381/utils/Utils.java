package no.uib.smo015.info381.utils;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Sindre on 21.04.2017.
 */
public class Utils {

    /*
        Returns a randomDouble number between two numbers
     */
    public static double randomDouble(double low, double high) {
        Random random = new Random();
        double randomDouble = low + ((high - low) * random.nextDouble());
        DecimalFormat decimalFormat = new DecimalFormat(".00");
        return Double.parseDouble(decimalFormat.format(randomDouble).replace(",", "."));

    }

    public static int randomInt(int low, int high) {
        Random random = new Random();
        return random.nextInt(high - low) + low;
    }
}
